import {getLocalUser} from "../helpers/auth";
import jwt from 'jsonwebtoken'
import publicKey from "../jwt/public";
import products from './modules/products.js';
import axios from "axios";

const user = getLocalUser();


export default {

	modules: {
		products
	},

	state:{
		currentUser: user,
		isLoggedIn: !!user,
		loading: false,
		auth_error: null,
		tokenSaver : null,
	},


	getters:{
		isLoading(state){
			return state.loading
		},

		isLoggedIn(state){
			return state.isLoggedIn
		},

		currentUser(state) {
			return state.currentUser;
		},

		auth_error(state){
			return state.auth_error
		},
		tokenSaver(state){
			return state.tokenSaver
		}
	},

	mutations:{
		login(state){
			state.loading = true;
			state.auth_error = null;
		},

		loginSuccess(state, payload){
			state.auth_error = null;
			state.isLoggedIn = true;
			state.loading = false;

			let test = jwt.verify(payload.access_token, publicKey)

			state.currentUser = Object.assign({}, test, {token: payload.access_token});
			localStorage.setItem("user", JSON.stringify(this.state.currentUser));

		},

		loginFailed(state, payload){
			state.loading = false;
			state.auth_error = payload;
		},

		logout(state){
			localStorage.removeItem("user");
			state.isLoggedIn = false;
			state.currentUser = null;
		}
	},

	actions:{
		login(context){
			context.commit("login");
		}
	},
}
