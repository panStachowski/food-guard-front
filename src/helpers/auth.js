import { setAuthorization } from "./general";
import axios from 'axios'
//import Cookies from 'js-cookie'
import * as types from '../helpers/mutation-types'


export function login(credentials) {
    return new Promise((res, rej) => {
        axios.post('login', credentials)
            .then((response) => {
                console.log(response)
                setAuthorization(response.data.access_token);
                res(response.data);
            })
            .catch((err) =>{
                rej("Wrong email or password");
            })
    })

}

export function getLocalUser() {
    const userStr = localStorage.getItem("user");

    if(!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}

// state
export const state = {
    user: null,
    //token: Cookies.get('token')
}


// getters
export const getters = {
    user: state => state.user,
    check: state => state.user !== null,
    token: state => state.token
}

// mutations
export const mutations = {
    [types.UPDATE_USER] (state, { user }) {
        state.user = user
    },
    [types.SAVE_TOKEN] (state, { token, remember }) {
        state.token = token
        //Cookies.set('token', token, { expires: remember ? 365 : null })
    }


}

// actions
export const actions = {
    updateUser ({ commit }, payload) {
        commit(types.UPDATE_USER, payload)
    },
    saveToken ({ commit, dispatch }, payload) {
        commit(types.SAVE_TOKEN, payload)
    }

}

