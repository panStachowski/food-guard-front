import axios from 'axios'



export function initialize(store, router) {
    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const currentUser = store.state.currentUser;

        if(requiresAuth && !currentUser) {
            next('/login');
        } else if(to.path == '/login' && currentUser) {
            next('/');
        }
        else {
            next();
        }
    });

    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const currentUser = store.state.currentUser;

        if(requiresAuth && !currentUser) {
            next('/register');
        } else if(to.path == '/register' && currentUser) {
            next('/');
        }
        else {
            next();
        }
    });

    axios.interceptors.response.use(null, (error) => {
        if (error.response.status !== 200 && error.response.status !== 500) {
            alert(JSON.stringify(error.response.data.message))
            console.log(JSON.stringify(error.response.data.message))
        }
        if(error.response.data.message === 'Access denied' || error.response.data.message === 'Nie masz uprawnień'){
            router.push('/');
        }
        if(error.response.data.message === 'Niewłaściwy token' || error.response.data.message === 'Invalid token'){
            store.commit('logout');
            router.push('/');
        }

        return Promise.reject(error);
    });

    if (store.getters.currentUser) {
        setAuthorization(store.getters.currentUser.token);
    }

}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}
