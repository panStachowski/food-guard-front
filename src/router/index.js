import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../components/auth/Login.vue'
import Register from "../components/auth/Register.vue"
import Meals from "../views/Meals.vue";
import ProductPanel from '../views/ProductPanel.vue'
import TestComponent from '../views/TestComponent'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      quest: true
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
  },
  {
    path: '/meals',
    name: 'Meals',
    component: Meals,
  },
    {
        path: '/productPanel',
        name: 'productPanel',
        component: ProductPanel,
    },
    {
        path: '/test',
        name: 'Test',
        component: TestComponent,
    },
];

const router = new VueRouter( {
	mode: 'history',
  routes,
});

export default router
