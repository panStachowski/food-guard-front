import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import storeData from './store'
import vuetify from './plugins/vuetify';
import env from './env.js'
import axios from 'axios'
import {initialize} from "./helpers/general";
import Vuex from 'vuex'
import Vuelidate from 'vuelidate'


import {getLocalUser} from "./helpers/auth";
Vue.use(Vuex);
Vue.use(Vuelidate)

axios.defaults.baseURL = env.API_URL;
window.axios = axios;
const store = new Vuex.Store(storeData);
initialize(store, router);



Vue.config.productionTip = false;

export const eventBus = new Vue();


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
